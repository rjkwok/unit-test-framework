CC = g++
FLAGS = -std=c++11
SOURCES = main.cpp
OUTPUT = main.exe

all:
	$(CC) $(FLAGS) $(SOURCES) -o $(OUTPUT)