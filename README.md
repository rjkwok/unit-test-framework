testing.h

- Header-only library providing lightweight macros for defining bare-bones unit tests as an alternative to gtest
- Currently only supports windows due to terminal function dependenchy
- TEST(test_name) declares and registers a unit test as an independent function
- RUN_ALL_TESTS() executes all registered tests and prints all results to std::cout

main.cpp

- Example use of testing.h

makefile

- simple g++ build for main.cpp example

EXAMPLE OUTPUT:

	[==========] Running 2 tests.
	[  RUN     ] TestMultiplication
	[----------] Assertion passed: multiply(0, 1) == 0
	[----------] Assertion passed: multiply(1, 1) == 1
	[----------] Assertion passed: multiply(2, 2) == 4
	[----------] Assertion passed: multiply(3, 3) == 9
	[  PASSED  ] TestMultiplication
	[  RUN     ] TestAddition
	[----------] Assertion failed: multiply(0, 1) == 1
	[----------] Actual: 0
	[----------] Expected: 1
	[----------] Assertion failed: multiply(1, 1) == 2
	[----------] Actual: 1
	[----------] Expected: 2
	[----------] Assertion passed: multiply(2, 2) == 4
	[----------] Assertion failed: multiply(3, 3) == 6
	[----------] Actual: 9
	[----------] Expected: 6
	[  FAILED  ] TestAddition

	 1 FAILED TESTS