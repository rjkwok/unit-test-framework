/*
	testing.h

	- Header-only library providing lightweight macros for defining bare-bones unit tests as an alternative to gtest
	- Currently only supports windows due to terminal function dependenchy
	- TEST(test_name) declares and registers a unit test as an independent function
	- RUN_ALL_TESTS() executes all registered tests and prints all results to std::cout

	Author: Richard Kwok
	Date: 2019/08/14

	EXAMPLE:

		#include "testing.h"

		int multiply(int a, int b)
		{
			return a*b;
		}

		TEST(TestMultiplication)
		{
			ASSERT_EQ(multiply(0, 1), 0)
			ASSERT_EQ(multiply(1, 1), 1)
			ASSERT_EQ(multiply(2, 2), 4)
			ASSERT_EQ(multiply(3, 3), 9)
		}

		TEST(TestAddition) // Will fail since the multiply function is being used
		{
			ASSERT_EQ(multiply(0, 1), 1)
			ASSERT_EQ(multiply(1, 1), 2)
			ASSERT_EQ(multiply(2, 2), 4)
			ASSERT_EQ(multiply(3, 3), 6)
		}

		int main()
		{
			RUN_ALL_TESTS()
			return 0;
		}

	OUTPUT:

		[==========] Running 2 tests.
		[  RUN     ] TestMultiplication
		[----------] Assertion passed: multiply(0, 1) == 0
		[----------] Assertion passed: multiply(1, 1) == 1
		[----------] Assertion passed: multiply(2, 2) == 4
		[----------] Assertion passed: multiply(3, 3) == 9
		[  PASSED  ] TestMultiplication
		[  RUN     ] TestAddition
		[----------] Assertion failed: multiply(0, 1) == 1
		[----------] Actual: 0
		[----------] Expected: 1
		[----------] Assertion failed: multiply(1, 1) == 2
		[----------] Actual: 1
		[----------] Expected: 2
		[----------] Assertion passed: multiply(2, 2) == 4
		[----------] Assertion failed: multiply(3, 3) == 6
		[----------] Actual: 9
		[----------] Expected: 6
		[  FAILED  ] TestAddition

		 1 FAILED TESTS
*/

#ifndef RJK_TESTING_H
#define RJK_TESTING_H

#include <memory>
#include <chrono>
#include <map>
#include <string>
#include <iostream>
#include <windows.h>

namespace testing
{

class Test;

// Global test suite container
std::map<std::string, std::unique_ptr<Test> > tests;

// Each test definition must be a subclass of this abstract base
class Test
{
	// Flip test state instead of returning immediately upon assertion, so that a single assertion doesn't halt testing
	bool failed = false;

public:
	void fail(void)
	{
		failed = true;
	}

	bool getFailed(void)
	{
		return failed;
	}

	virtual void run(void) = 0;
};

void runAllTests()
{
	int tests_failed = 0;

	// For switching between windows console text attributes: 7 = normal (grey), 10 = green, 12 = red
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 7);

	std::cout << "[==========] " << "Running " << tests.size() << " tests." << std::endl;
	for (auto it = tests.begin(); it != tests.end(); it++)
	{
		SetConsoleTextAttribute(hConsole, 10);
		std::cout << "[  RUN     ] ";
		SetConsoleTextAttribute(hConsole, 7);
		std::cout << it->first << std::endl;

		auto start = std::chrono::high_resolution_clock::now();
		it->second->run();
		auto end = std::chrono::high_resolution_clock::now();

		// Test duration in milliseconds with three decimal places of precision
		double test_duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()*1e-3;

		if (it->second->getFailed())
		{
			tests_failed++;
			SetConsoleTextAttribute(hConsole, 12);
			std::cout << "[  FAILED  ] ";
			SetConsoleTextAttribute(hConsole, 7);
			std::cout << it->first << " (" << test_duration << " ms)" << std::endl;
		}
		else
		{
			SetConsoleTextAttribute(hConsole, 10);
			std::cout << "[  PASSED  ] ";
			SetConsoleTextAttribute(hConsole, 7);
			std::cout << it->first << " (" << test_duration << " ms)" << std::endl;
		}
	}
	std::cout << std::endl << " " << tests_failed << " FAILED TESTS" << std::endl;
}

}

/*
	Each test is a unique singleton subclass. Each test subclass type must be instantiated in the testing::tests map
	to "register" the test for execution. To do so before entering main, an unnecessary static bool is dynamically
	initialized with the real purpose of executing the registerTest() method for each sublcass to load an instance of
	each subclass into the map. This is all taken care of in the TEST(test_name) macro, so the only thing the user must
	implement is the body of the test execution in the run() method of each test subclass.
*/

#define TEST(test_name) 															\
class test_name: public testing::Test 												\
{																					\
public:																				\
	static bool registered;															\
																					\
	static bool registerTest(void)													\
	{																				\
		testing::tests[#test_name] = std::unique_ptr<test_name>(new test_name());	\
	}																				\
																					\
	void run(void) override;														\
};																					\
																					\
bool test_name::registered = test_name::registerTest();								\
																					\
void test_name::run(void)

#define ASSERT_EQ(actual, expected)																		\
																										\
	if (actual == expected)																				\
	{																									\
		std::cout << "[----------] Assertion passed: " << #actual << " == " << #expected << std::endl;	\
	}																									\
	else																								\
	{																									\
		std::cout << "[----------] Assertion failed: " << #actual << " == " << #expected << std::endl;	\
		std::cout << "[----------] Actual: " << (actual) << std::endl;									\
		std::cout << "[----------] Expected: " << (expected) << std::endl;								\
		this->fail();																					\
	}

#define RUN_ALL_TESTS() testing::runAllTests();

#endif