/*
	testing.h example

	Author : Richard Kwok
	Date : 2018/08/14
*/

#include "testing.h"

int multiply(int a, int b)
{
	return a*b;
}

TEST(TestMultiplication)
{
	ASSERT_EQ(multiply(0, 1), 0)
	ASSERT_EQ(multiply(1, 1), 1)
	ASSERT_EQ(multiply(2, 2), 4)
	ASSERT_EQ(multiply(3, 3), 9)
}

TEST(TestAddition) // Will fail since the multiply function is being used
{
	ASSERT_EQ(multiply(0, 1), 1)
	ASSERT_EQ(multiply(1, 1), 2)
	ASSERT_EQ(multiply(2, 2), 4)
	ASSERT_EQ(multiply(3, 3), 6)
}

int main()
{
	RUN_ALL_TESTS()
	return 0;
}